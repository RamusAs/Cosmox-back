require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const lyricsFinder = require('lyrics-finder');
const SpotifyWebApi = require('spotify-web-api-node');
const { server, shopify } = require('./config');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function getBaseUrlCLient(req) {
  const baseUrl = req.headers.origin;
  return baseUrl;
}

app.get('/ping', (req, res) => {
  return res.json({ content: 'ping' });
});

app.post('/refresh', async (req, res) => {
  const { refreshToken } = req.body;
  const spotifyApi = new SpotifyWebApi({
    ...shopify,
    redirectUri: getBaseUrlCLient(req),
    refreshToken,
  });

  try {
    const { access_token, refresh_token } = (await spotifyApi.refreshAccessToken()).body;
    return res.json({
      accessToken: access_token,
      expiresIn: refresh_token,
    });
  } catch (error) {
    return res.sendStatus(400);
  }
});

app.post('/login', async (req, res) => {
  const { code } = req.body;
  const spotifyApi = new SpotifyWebApi({
    ...shopify,
    redirectUri: getBaseUrlCLient(req),
  });

  try {
    const { access_token, refresh_token, expires_in } = (await spotifyApi.authorizationCodeGrant(code)).body;
    return res.json({
      accessToken: access_token,
      refreshToken: refresh_token,
      expiresIn: expires_in,
    });
  } catch (error) {
    console.log('🚀 - file: server.js:53 - error:', error);
    return res.sendStatus(400);
  }
});

app.get('/lyrics', async (req, res) => {
  const { artist, track } = req.query;
  try {
    const lyrics = (await lyricsFinder(artist, track)) || 'No Lyrics Found';
    return res.json({ lyrics });
  } catch (error) {
    return res.sendStatus(400);
  }
});

app.listen(server.port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on ${server.hostname}: ${server.port}`);
});
