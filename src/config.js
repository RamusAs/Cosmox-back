const dotenv = require('dotenv');

dotenv.config();

const SERVER = {
  hostname: process.env.HOST || 'localhost',
  port: process.env.PORT || 3001,
};

const SHOPIFY = {
  clientId: process.env.SHOPIFY_CLIENT_ID,
  clientSecret: process.env.SHOPIFY_CLIENT_SECRET,
};


const config = {
  server: SERVER,
  shopify: SHOPIFY,
  env: process.env.NODE_ENV || 'development',
};

module.exports = config;
